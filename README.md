WUSTL Email Templates
=====================

A set of boilerplates for quickly creating HTML email templates that respect the current brand guidelines from the Office of Public Affairs.

## Basic Styles

* [Basic Example](email.html)
* [Formal Example](email-formal.html)

## Instructions

This is a project that is meant to hack, so start by forking this repo into your own project and build your own templates from there.

### Requirements
In order to use these templates, you'll need to have the following installed on your machine:

* Node.js >= 0.10.0 ([install wiki](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager))
* Grunt-cli >= 0.1.7 and Grunt >=0.4.2 (npm install grunt-cli -g)
* Ruby >= 1.8.7 ([installers](http://www.ruby-lang.org/en/downloads/))
* Premailer >= 1.8.0 (gem install premailer and, most of the time, gem install hpricot)

### Getting Started

The first time you open a project, run `npm install` from the root folder of the project to automatically download and install all of the dependencies that are listed in the `package.json` file.

### Using the Grunt Tasks

There are several commands pre-configured to make your life easier. They are as follows:

* `grunt` – The default task, which cleans out your current dist directory, autoprefixes your CSS, inlines the CSS into your HTML templates, and outputs the inlined HTML into the `/dist` directory.
* `grunt serve` – This task automatically opens your browser to the index page in the source directory, from which you can navigate to your HTML email file and watch it auto-update when you save the file.
* `ground serve:dist` - This task pre-builds your templates using the default task, and then open the project in your browser. **Note:** this task doesn't auto-refresh if you make changes.

### Tips to producing production-ready emails

Before sending out the email you should check for the following common issues:

* Any images that are wider than 550px will break the layout. Just don't.
* All images must be hosted on a web accessible server (e.g., https://emailcontent.wustl.edu) and should have correct height, width, and alt attributes added:

**DO**

```
<img src="https://emailcontent.wustl.edu/WU_PA/logo-wustl-4-color-rev-shield.png" alt="Washington University in St. Louis" height="27" width="321" />
```

**DON’T**

```
<img src="/logo-wustl-4-color-rev-shield.png" /> <-- this is sad :(
```


* Uncomment the following block in the `<head>` of the HTML **after** running the default grunt process (i.e. do this to the HTML in the `dist` folder):

```
<!-- <style type="text/css">
    .ios-white a {
        color: #FFFFFF !important;
        text-decoration: none !important;
    }

    .ios-red a {
        color: #9B0B15 !important;
        text-decoration: none !important;
    }

    .ios-black a {
        color: #333333 !important;
        text-decoration: none !important;
    }
</style> -->
```

* Make sure to update all contact add unsubscribe information in the footer of the email.
* Finally, run the final code through an email testing service like [Litmus.com](http://litmus.com) to check for any problems with different email clients before sending out.